//
// Created by peter on 6/25/18.
//

#include "KeyboardEmulator.h"

bool KeyboardEmulator::getKeyState(int keyCode) {
    return keys[keyCode];


}

void KeyboardEmulator::update() {
    if(counter % 60*5 == 0 ){
        counter = 0;
        state++;

        if(state == states.size()){
            state = 0;
        }

        setState();

    }
    counter++;
}

KeyboardEmulator::KeyboardEmulator():keys(256, false) {

    states = {
            {{LEFT, false}, {RIGHT, false}, {UP, true}, {DOWN, false}},
            {{LEFT, false}, {RIGHT, false}, {UP, true}, {DOWN, false}},
            {{LEFT, false}, {RIGHT, false}, {UP, true}, {DOWN, false}},
            {{LEFT, true}, {RIGHT, false}, {UP, false}, {DOWN, true}},
            {{LEFT, true}, {RIGHT, false}, {UP, true}, {DOWN, false}},
            {{LEFT, false}, {RIGHT, true}, {UP, false}, {DOWN, true}},
            {{LEFT, false}, {RIGHT, false}, {UP, true}, {DOWN, false}}

    };
    setState();

}

void KeyboardEmulator::setState() {
    auto st = states[state];

    for(auto s : st){
        keys[s.first] =s.second;
    }
}
