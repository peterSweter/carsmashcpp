//
// Created by peter on 6/25/18.
//

#include "BotManager.h"

BotManager::BotManager(CarFactory *carFactory_) : carFactory_(carFactory_) {
    for(int i=0; i < this->botCount; i++){
        botVec.push_back(std::make_shared<BotSession>(carFactory_->create("botModel")));
    }
}

void BotManager::update() {
    for(int i = 0; i <  botVec.size(); i++){
        auto & bot = botVec[i];

        if(bot->isAlive()){
            bot->update();
        }else{

            botVec[i] = std::make_shared<BotSession>(carFactory_->create("botModel"));
        }
    }
}
