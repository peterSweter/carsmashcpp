//
// Created by peter on 6/25/18.
//

#ifndef CARSMASHCPP_BOTSESSION_H
#define CARSMASHCPP_BOTSESSION_H


#include "../player/PlayerI.h"
#include "KeyboardEmulator.h"
#include "../entities/car/Car.h"

class BotSession : PlayerI {
public:

    BotSession(const std::shared_ptr<Car> &car_);

    std::shared_ptr<Car> car_;
    KeyboardEmulator keboardEmulator;
    std::string getNickname() override;

    b2Vec2 getPosition() override;

    double getScore() override;
    void update();

    bool isAlive();

    ~BotSession();

};


#endif //CARSMASHCPP_BOTSESSION_H
