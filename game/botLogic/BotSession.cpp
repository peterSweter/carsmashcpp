//
// Created by peter on 6/25/18.
//

#include "BotSession.h"

std::string BotSession::getNickname() {
    return "bot";
}

b2Vec2 BotSession::getPosition() {
    return b2Vec2();
}

double BotSession::getScore() {
    return 0;
}

BotSession::BotSession(const std::shared_ptr<Car> &car_) : car_(car_) {
    car_->setKeyboardManager(&keboardEmulator);
    car_->setPlayer(this);
}

void BotSession::update() {
    this->car_->update();
    this->keboardEmulator.update();
}

bool BotSession::isAlive() {
    return this->car_->isAlive();
}

BotSession::~BotSession() {
    std::cout << "Bot deleted" << std::endl;
    car_.reset();
}
