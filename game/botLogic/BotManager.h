//
// Created by peter on 6/25/18.
//

#ifndef CARSMASHCPP_BOTMANAGER_H
#define CARSMASHCPP_BOTMANAGER_H


#include "../entities/car/Car.h"
#include "KeyboardEmulator.h"
#include "../entities/car/CarFactory.h"
#include "BotSession.h"

class BotManager {
public:
    CarFactory *carFactory_;
    int botCount = 3;

    void update();
    std::vector<std::shared_ptr<BotSession>> botVec;

    BotManager(CarFactory *carFactory_);
};


#endif //CARSMASHCPP_BOTMANAGER_H
