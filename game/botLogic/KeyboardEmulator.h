//
// Created by peter on 6/25/18.
//

#ifndef CARSMASHCPP_KEYBOARDEMULATOR_H
#define CARSMASHCPP_KEYBOARDEMULATOR_H


#include "../input/KeyboardManagerI.h"
#include <vector>

class KeyboardEmulator : public KeyboardManagerI {
    std::vector<bool> keys;
    int counter = 0;
    int state = 0 ;

    std::vector<std::vector<std::pair<int, bool>>> states;

public:

    bool getKeyState(int keyCode) override;
    void update();

    KeyboardEmulator();

    static const int LEFT = 37;
    static const int UP = 40;
    static const int RIGHT = 39;
    static const int DOWN = 38;

    void setState();


};


#endif //CARSMASHCPP_KEYBOARDEMULATOR_H
