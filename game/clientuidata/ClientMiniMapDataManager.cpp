//
// Created by peter on 6/17/18.
//

#include "ClientMiniMapDataManager.h"

std::shared_ptr<Json> ClientMiniMapDataManager::getJsonData(std::vector<std::shared_ptr<Player>> &activePlayersList) {
    auto jsonData = std::make_shared<Json>();

    jsonData->emplace("t", "mp"); // top players tats

    Json positionsJsonArray;

    for(auto &  player : activePlayersList ){

        Json positionRecord;

        auto position = player->getPosition();

        positionRecord.emplace("x", position.x);
        positionRecord.emplace("y", position.y);


        positionsJsonArray.push_back(positionRecord);

    }

    jsonData->emplace("positions_table", positionsJsonArray);


    return jsonData;

}
