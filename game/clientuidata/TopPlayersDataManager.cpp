//
// Created by peter on 6/17/18.
//

#include "TopPlayersDataManager.h"

std::shared_ptr<Json> TopPlayersDataManager::getJsonData(std::vector<std::shared_ptr<Player>> &activePlayersList) {

    std::sort(activePlayersList.begin(), activePlayersList.end(), playerScoreCmp);

    auto jsonData = std::make_shared<Json>();

    jsonData->emplace("t", "tps"); // top players tats

    Json scoreJsonArray;

    for(int i=0 ; i < TOP_SCORE_TABLE_SIZE && i < activePlayersList.size(); i++){
        Json scoreRecord;

        auto & player  = activePlayersList[i];

        scoreRecord.emplace("nick", player->getNickname());
        scoreRecord.emplace("score", std::to_string(player->getScore()));

        scoreJsonArray.push_back(scoreRecord);
    }

    jsonData->emplace("score_table", scoreJsonArray);


    return jsonData;


}


std::function<bool(std::shared_ptr<Player> &, std::shared_ptr<Player> &)> TopPlayersDataManager::playerScoreCmp = [](std::shared_ptr<Player> & a , std::shared_ptr<Player> & b ){
    return a->getScore() > b->getScore();
};