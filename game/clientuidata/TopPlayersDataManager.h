//
// Created by peter on 6/17/18.
//

#ifndef CARSMASHCPP_TOPPLAYERSDATAMANAGER_H
#define CARSMASHCPP_TOPPLAYERSDATAMANAGER_H


#include <list>
#include "../player/Player.h"

class TopPlayersDataManager {

public:

    std::shared_ptr<Json>  getJsonData(std::vector<std::shared_ptr<Player>> & activePlayersList);
    static std::function<bool(std::shared_ptr<Player> & , std::shared_ptr<Player> &)> playerScoreCmp;
    const int TOP_SCORE_TABLE_SIZE = 10;

};


#endif //CARSMASHCPP_TOPPLAYERSDATAMANAGER_H
