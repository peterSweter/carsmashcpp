//
// Created by peter on 6/17/18.
//

#ifndef CARSMASHCPP_CLIENTMINIMAPDATAMANAGER_H
#define CARSMASHCPP_CLIENTMINIMAPDATAMANAGER_H


#include <list>
#include "../player/Player.h"

class ClientMiniMapDataManager {

public:
    std::shared_ptr<Json>  getJsonData(std::vector<std::shared_ptr<Player>> & activePlayersList);
};


#endif //CARSMASHCPP_CLIENTMINIMAPDATAMANAGER_H
