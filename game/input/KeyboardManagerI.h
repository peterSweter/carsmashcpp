//
// Created by peter on 6/25/18.
//

#ifndef CARSMASHCPP_KEYBOARDMANAGERI_H
#define CARSMASHCPP_KEYBOARDMANAGERI_H


class KeyboardManagerI {
public:
    virtual bool getKeyState(int keyCode) = 0;
};


#endif //CARSMASHCPP_KEYBOARDMANAGERI_H
