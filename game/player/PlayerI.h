//
// Created by peter on 5/4/18.
//

#ifndef CARSMASHCPP_PLAYERI_H
#define CARSMASHCPP_PLAYERI_H


#include <string>
#include <Box2D/Common/b2Math.h>

class PlayerI {

    public:
    virtual std::string getNickname() = 0;
    virtual b2Vec2 getPosition() = 0;
    virtual double getScore() = 0;
};


#endif //CARSMASHCPP_PLAYERI_H
