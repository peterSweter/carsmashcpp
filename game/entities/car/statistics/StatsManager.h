//
// Created by peter on 6/26/18.
//

#ifndef CARSMASHCPP_STATSMANAGER_H
#define CARSMASHCPP_STATSMANAGER_H

#include <vector>
#include <nlohmann/json.hpp>


using Json = nlohmann::json;

class StatsManager {
private:

    std::vector<int> levels;
    std::vector<double> values;
    std::vector<double> upgradeMagnitude;
    std::map<std::string, int> statKeys = {{"maxHealth",    0},
                                           {"healthRegen",  1},
                                           {"speed",        2},
                                           {"acceleration", 3}
    };




    int maxStatLevel = 5;
    int level = 0;

    int maxLevel= 12;

    double exp = 0;
    double expForLevel = 50;

    int upgradePoints = 2;


    const int MAX_HP_ID = 0;
    const int HP_REGEN_ID = 1;
    const int SPEED_ID = 2;
    const int ACCELERATION_ID = 3;

    const int statCount = 4;

public:

    StatsManager(Json &json);

    void gainExp(double value);

    void handleMessage(std::shared_ptr<Json> msg);

    double getMaxHealth();

    double getSpeed();

    double getAcceleration();

    double getHealthRegen();

    double getPercentageExp();

    int getUpgradePoints();
    int getLevel();

    Json getJsonData();


};


#endif //CARSMASHCPP_STATSMANAGER_H
