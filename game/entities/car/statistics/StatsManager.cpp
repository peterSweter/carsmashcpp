//
// Created by peter on 6/26/18.
//

#include <iostream>
#include "StatsManager.h"

void StatsManager::gainExp(double value) {

    if(level < maxLevel){
        this->exp += value;

        if(level * expForLevel <  this->exp){
            std::cout << "level------------------------ up current level: " <<  level <<  std::endl;
            level++;
            upgradePoints++;
        }

    }

}

StatsManager::StatsManager(Json &json) {



    values = std::vector<double>(4);
    levels = std::vector<int>(4,0);
    upgradeMagnitude = std::vector<double>(4);

    auto baseStats = json["base"];
    auto upgradeJson = json["upgradeMagnitude"];

    for(auto  it = baseStats.begin(); it!= baseStats.end(); ++it) {
        values[statKeys[it.key()]] = it.value().get<double>();
    }

    for(auto  it = upgradeJson.begin(); it!= upgradeJson.end(); ++it) {
        upgradeMagnitude[statKeys[it.key()]] = it.value().get<double>();
    }


}

double StatsManager::getMaxHealth() {
    return values[MAX_HP_ID];
}

double StatsManager::getSpeed() {
    return values[SPEED_ID];
}

double StatsManager::getAcceleration() {
    return values[ACCELERATION_ID];
}

double StatsManager::getHealthRegen() {
    return values[HP_REGEN_ID];
}

double StatsManager::getPercentageExp() {
    double res =  exp / (expForLevel*(double)(level+1));
    //std::cout << res << std::endl;
    return res;
}

int StatsManager::getUpgradePoints() {
    return upgradePoints;
}

int StatsManager::getLevel() {
    return level;
}

void StatsManager::handleMessage(std::shared_ptr<Json> msg) {
    if(upgradePoints > 0){
        auto it = msg->find("v");
        auto key =  it->get<int>();


        if(levels[key] < maxStatLevel){
            std::cout <<"upgrade key: " << key << " upgradePoints: " << upgradePoints <<  std::endl;
            values[key] += upgradeMagnitude[key];
            levels[key] ++;
            auto test =  getJsonData();
            std::cout << "json test " << test.dump() << std::endl;
            upgradePoints--;
        }
    }
}

Json StatsManager::getJsonData() {

    Json data;

    for(auto it : statKeys){

       data.emplace(it.first, levels[it.second]);

    }


    data.emplace("upgradePoints", upgradePoints);
   // std::cout << "stat json:  " << data.dump() << std::endl;
    return data;
}
