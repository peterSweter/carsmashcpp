//
// Created by peter on 6/17/18.
//

#include "ArenaBoundariesManager.h"


ArenaBoundariesManager::ArenaBoundariesManager(Box2dManager *box2dManager) {
    // constructing boundaries

    for(float x= -width_/2 + segmentMaxWidth_/2; x<= width_/2 - segmentMaxWidth_/4; x+= segmentMaxWidth_){
        segments_.push_back(std::make_unique<BoundSegment>(box2dManager, segmentMaxWidth_/2, segmentThickness_, b2Vec2(x, heigt_/2 + segmentThickness_)));
        segments_.push_back(std::make_unique<BoundSegment>(box2dManager, segmentMaxWidth_/2, segmentThickness_, b2Vec2(x, -heigt_/2 - segmentThickness_)));
    }

    for(float y= heigt_/2 - segmentMaxHeight_/2 ; y>= -heigt_/2 + segmentMaxHeight_/4; y-= segmentMaxHeight_){
        segments_.push_back(std::make_unique<BoundSegment>(box2dManager, segmentThickness_, segmentMaxHeight_/2, b2Vec2(-width_/2, y)));
        segments_.push_back(std::make_unique<BoundSegment>(box2dManager, segmentThickness_, segmentMaxHeight_/2, b2Vec2(width_/2, y)));
    }
}
