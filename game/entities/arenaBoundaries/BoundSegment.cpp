//
// Created by peter on 6/17/18.
//

#include "BoundSegment.h"
#include "../../box2D/interactionManager/collideMasks.h"

BoundSegment::BoundSegment(Box2dManager *box2dManager, float width, float height, b2Vec2 pos) : box2dManager_(
        box2dManager), pos_(pos), width_(width), height_(height) {

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(pos.x, pos.y);

    b2FixtureDef fixtureDef;
    b2PolygonShape polygonShape;
    polygonShape.SetAsBox(width, height);
    fixtureDef.shape = &polygonShape;

    body_ = box2dManager_->createBody(&bodyDef);
    auto fix = body_->CreateFixture(&fixtureDef);
    body_->SetUserData((DataCollectableOnceI*)this);
    fix->SetUserData((InteractiveEntityPartA*)this);

    this->setMyMask(collideMask::wall);
    this->setMaskCollideWith(0);

    this->setEntity(this);

}

BoundSegment::~BoundSegment() {
    box2dManager_->getGameWorld()->DestroyBody(body_);
}

std::shared_ptr<Json> BoundSegment::getJsonData() {

    auto jsonData = std::make_shared<Json>();
    jsonData->emplace("t", "boundSegment");
    jsonData->emplace("x", pos_.x);
    jsonData->emplace("y", pos_.y);

    Json bodiesJsonArray;

    Json bodyJsonObj;

    bodyJsonObj.emplace("x", pos_.x);
    bodyJsonObj.emplace("y", pos_.y);
    bodyJsonObj.emplace("angle", 0.0);

    Json fixturesArray;

    Json fixtureJson;

    fixtureJson.emplace("shape", "box");
    fixtureJson.emplace("color", this->color_);
    fixtureJson.emplace("x", 0);
    fixtureJson.emplace("w", 0);
    fixtureJson.emplace("width", width_);
    fixtureJson.emplace("height", height_);
    fixtureJson.emplace("angle", 0.0);

    fixturesArray.push_back(fixtureJson);
    bodyJsonObj.emplace("fixtures", fixturesArray);
    bodiesJsonArray.push_back(bodyJsonObj);
    jsonData->emplace("bodies", bodiesJsonArray);

    return jsonData;



}

void BoundSegment::update() {

}

void BoundSegment::dealDamage(InteractiveEntityPartA *entity, double dmg) {

}
