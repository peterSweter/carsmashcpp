//
// Created by peter on 6/17/18.
//

#ifndef CARSMASHCPP_ARENABOUNDARIES_H
#define CARSMASHCPP_ARENABOUNDARIES_H


#include "BoundSegment.h"

class ArenaBoundariesManager {
private:

    const float width_ = 20.0;
    const float heigt_ = 20.0;
    const float segmentMaxWidth_ = 10.0;
    const float segmentMaxHeight_ = 10.0;
    const float segmentThickness_ = 0.2;

    std::vector<std::unique_ptr<BoundSegment>> segments_;

public:

    ArenaBoundariesManager(Box2dManager *box2dManager);
};


#endif //CARSMASHCPP_ARENABOUNDARIES_H
