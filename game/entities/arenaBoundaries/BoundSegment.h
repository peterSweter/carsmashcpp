//
// Created by peter on 6/17/18.
//

#ifndef CARSMASHCPP_BOUNDSEGMENT_H
#define CARSMASHCPP_BOUNDSEGMENT_H


#include "../../box2D/Box2dManager.h"
#include "../../dataCollector/DataCollectableI.h"
#include "../../box2D/interactionManager/InteractiveEntityPartA.h"
#include "../../dataCollector/DataCollectableOnceI.h"


class BoundSegment : public DataCollectableOnceI, public InteractiveEntityPartA, public EntityI {
private:

    std::string color_ = "brown";
    Box2dManager *box2dManager_;
    b2Body * body_;
    b2Vec2 pos_;
    float width_, height_;

public:

    BoundSegment(Box2dManager *box2dManager, float width, float height, b2Vec2 pos);
    ~BoundSegment();

    std::shared_ptr<Json> getJsonData() override;

    void update() override;

    void dealDamage(InteractiveEntityPartA *entity, double dmg) override;

};


#endif //CARSMASHCPP_BOUNDSEGMENT_H
